

import * as THREE from 'three';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';

window.THREE = THREE;
window.OrbitControls = OrbitControls;

