package com.java110.bean.po;

public class BasePo {
    private String oprUserId;
    private String oprUserName;
    private String tel;

    public String getOprUserId() {
        return oprUserId;
    }

    public void setOprUserId(String oprUserId) {
        this.oprUserId = oprUserId;
    }

    public String getOprUserName() {
        return oprUserName;
    }

    public void setOprUserName(String oprUserName) {
        this.oprUserName = oprUserName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
