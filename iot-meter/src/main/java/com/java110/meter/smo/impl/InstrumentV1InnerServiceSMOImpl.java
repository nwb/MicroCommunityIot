/*
 * Copyright 2017-2020 吴学文 and java110 team.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.java110.meter.smo.impl;


import com.java110.bean.dto.PageDto;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.ApplicationContextFactory;
import com.java110.core.utils.Assert;
import com.java110.core.utils.BeanConvertUtil;
import com.java110.dto.hardwareManufacturer.HardwareManufacturerDto;
import com.java110.dto.mqtt.MqttResultDto;
import com.java110.intf.system.IHardwareManufacturerV1InnerServiceSMO;
import com.java110.meter.dao.IInstrumentV1ServiceDao;
import com.java110.intf.meter.IInstrumentV1InnerServiceSMO;
import com.java110.dto.instrument.InstrumentDto;
import com.java110.meter.factory.IInstrumentFactoryAdapt;
import com.java110.po.instrument.InstrumentPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 类表述： 服务之前调用的接口实现类，不对外提供接口能力 只用于接口建调用
 * add by 吴学文 at 2023-11-23 10:13:04 mail: 928255095@qq.com
 * open source address: https://gitee.com/wuxw7/MicroCommunity
 * 官网：http://www.homecommunity.cn
 * 温馨提示：如果您对此文件进行修改 请不要删除原有作者及注释信息，请补充您的 修改的原因以及联系邮箱如下
 * // modify by 张三 at 2021-09-12 第10行在某种场景下存在某种bug 需要修复，注释10至20行 加入 20行至30行
 */
@RestController
public class InstrumentV1InnerServiceSMOImpl implements IInstrumentV1InnerServiceSMO {

    @Autowired
    private IInstrumentV1ServiceDao instrumentV1ServiceDaoImpl;

    @Autowired
    private IHardwareManufacturerV1InnerServiceSMO hardwareManufacturerV1InnerServiceSMOImpl;

    private IInstrumentFactoryAdapt instrumentFactoryAdaptImpl;


    @Override
    public int saveInstrument(@RequestBody InstrumentPo instrumentPo) {
        int saveFlag = instrumentV1ServiceDaoImpl.saveInstrumentInfo(BeanConvertUtil.beanCovertMap(instrumentPo));
        return saveFlag;
    }

    @Override
    public int updateInstrument(@RequestBody InstrumentPo instrumentPo) {
        int saveFlag = instrumentV1ServiceDaoImpl.updateInstrumentInfo(BeanConvertUtil.beanCovertMap(instrumentPo));
        return saveFlag;
    }

    @Override
    public int deleteInstrument(@RequestBody InstrumentPo instrumentPo) {
        instrumentPo.setStatusCd("1");
        int saveFlag = instrumentV1ServiceDaoImpl.updateInstrumentInfo(BeanConvertUtil.beanCovertMap(instrumentPo));
        return saveFlag;
    }

    @Override
    public List<InstrumentDto> queryInstruments(@RequestBody InstrumentDto instrumentDto) {

        //校验是否传了 分页信息

        int page = instrumentDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            instrumentDto.setPage((page - 1) * instrumentDto.getRow());
        }

        List<InstrumentDto> instruments = BeanConvertUtil.covertBeanList(instrumentV1ServiceDaoImpl.getInstrumentInfo(BeanConvertUtil.beanCovertMap(instrumentDto)), InstrumentDto.class);

        return instruments;
    }


    @Override
    public int queryInstrumentsCount(@RequestBody InstrumentDto instrumentDto) {
        return instrumentV1ServiceDaoImpl.queryInstrumentsCount(BeanConvertUtil.beanCovertMap(instrumentDto));
    }

    @Override
    @Java110Transactional
    public String instrumentResult(@RequestBody MqttResultDto mqttResultDto) {
        HardwareManufacturerDto hardwareManufacturerDto = new HardwareManufacturerDto();
        hardwareManufacturerDto.setHmId(mqttResultDto.getHmId());
        hardwareManufacturerDto.setHmType(HardwareManufacturerDto.HM_TYPE_INSTRUMENT);
        List<HardwareManufacturerDto> hardwareManufacturerDtos = hardwareManufacturerV1InnerServiceSMOImpl.queryHardwareManufacturers(hardwareManufacturerDto);
        Assert.listOnlyOne(hardwareManufacturerDtos, "未找到仪表接口协议");
        instrumentFactoryAdaptImpl = ApplicationContextFactory.getBean(hardwareManufacturerDtos.get(0).getProtocolImpl(), IInstrumentFactoryAdapt.class);

        return instrumentFactoryAdaptImpl.instrumentResult(mqttResultDto.getTopic(), mqttResultDto.getData());
    }

    @Override
    public List<InstrumentDto> queryInstrumentList(InstrumentDto instrumentDto) {
        int page = instrumentDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            instrumentDto.setPage((page - 1) * instrumentDto.getRow());
        }

        List<InstrumentDto> instruments = BeanConvertUtil.covertBeanList(instrumentV1ServiceDaoImpl.getInstrumentInfo1(BeanConvertUtil.beanCovertMap(instrumentDto)), InstrumentDto.class);

        return instruments;
    }

}
